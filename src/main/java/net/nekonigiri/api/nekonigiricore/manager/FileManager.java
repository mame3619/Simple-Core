package net.nekonigiri.api.nekonigiricore.manager;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class FileManager {

    public static File getConfigFile(){
        return new File("plugins/SimpleCore","config.yml");
    }

    public static FileConfiguration getConfigFileConfiguration(){
        return YamlConfiguration.loadConfiguration(getConfigFile());
    }

    public static File getMySQLFile(){
        return new File("plugins/SimpleCore","mysql.yml");
    }

    public static FileConfiguration getMySQLFileConfiguration(){
        return YamlConfiguration.loadConfiguration(getMySQLFile());
    }

    public static void setStandardConfig(){
        FileConfiguration configuration = getConfigFileConfiguration();
        configuration.options().copyDefaults(true);
        try {
            configuration.save(getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readConfig(){
        FileConfiguration configuration = getConfigFileConfiguration();

    }

    public static void setStandardMySQL(){
        FileConfiguration mySQLConfiguration = getMySQLFileConfiguration();
        mySQLConfiguration.options().copyDefaults(true);
        mySQLConfiguration.addDefault("username","pluginuser");
        mySQLConfiguration.addDefault("password","SimplePVP");
        mySQLConfiguration.addDefault("database","simple-pvp");
        mySQLConfiguration.addDefault("host","localhost");
        mySQLConfiguration.addDefault("port","3306");
        try {
            mySQLConfiguration.save(getMySQLFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void readMySQL(){
        FileConfiguration mySQLConfiguration = getMySQLFileConfiguration();
        mySQLConfiguration.getString("");
    }
}

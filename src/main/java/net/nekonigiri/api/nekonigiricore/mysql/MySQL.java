package net.nekonigiri.api.nekonigiricore.mysql;

import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL {

    public static String username;
    public static String password;
    public static String database;
    public static String host;
    public static String port;
    public static Connection connection;

    public static void connect(){
        if(!isConnected()){
            try {
                connection = DriverManager.getConnection("jdbc:mysql://" + host + ":"  + port + "/" + database, username, password);
                if(isConnected())
                    Bukkit.getConsoleSender().sendMessage("MySQL Now Connected.");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(){
        if(isConnected()){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            Bukkit.getConsoleSender().sendMessage("MySQL Now Disconnected");

        }
    }

    public static boolean isConnected(){
        return connection != null;
    }

    public static void createTable(){
        try {
            connection.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS PlayerData (ID INTEGER(10000), PlayerName VARCHAR(100000), PlayerData VARCHAR(21845))");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}

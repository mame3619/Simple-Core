package net.nekonigiri.api.nekonigiricore;

import net.nekonigiri.api.nekonigiricore.manager.FileManager;
import net.nekonigiri.api.nekonigiricore.mysql.MySQL;
import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.TextChannel;
import org.bukkit.plugin.java.JavaPlugin;

import javax.security.auth.login.LoginException;

public final class SimpleCore extends JavaPlugin {

    public static SimpleCore simpleCore = new SimpleCore();
    public static JDA jda;
    public static TextChannel lobbyChannel;
    public static TextChannel informationChannel;

    @Override
    public void onEnable() {
        FileManager.getConfigFile();
        FileManager.getConfigFileConfiguration();
        FileManager.getMySQLFile();
        FileManager.getMySQLFileConfiguration();

        MySQL.connect();

        MySQL.createTable();
        discordEnable();
    }

    @Override
    public void onDisable() {
        MySQL.close();
    }

    public void discordEnable(){
        try {
            jda = new JDABuilder(AccountType.BOT).setToken("NDYyNDQ3MDMyODYzNDI0NTEy.Dhh--w.D4jTyXp0A7pF7TUIHv5Gp_Nlgdo").buildAsync();
        } catch (LoginException e) {
            e.printStackTrace();
        }

        TextChannel lChannel = jda.getTextChannelById(462456238698004490L);
        if(lobbyChannel.canTalk())
            lobbyChannel = lChannel;


    }

}

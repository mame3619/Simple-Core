package net.nekonigiri.api.nekonigiricore.entity;

import net.nekonigiri.api.nekonigiricore.SimpleCore;
import net.nekonigiri.api.nekonigiricore.enums.Permission;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.List;
@Data
public abstract class SinpleEntity {

    private Entity bukkitEntity;

    public Integer kills = 0;
    public Integer deaths = 0;
    public Integer exp = 0;
    public Integer dp = 0;
    public Integer rate = 0;

    public Integer won = 0;
    public Integer loses = 0;

    public Integer id = 0;
    public String joinedTeam;

    public boolean isMuteki = false;
    public boolean isMoving = false;

    public List<Permission> permissions;

    public Location lastMove;

    public Integer tickCounter = 0;
    public boolean offlineData = true;

    public Long DiscordID;

    public abstract void onTick();

    public SinpleEntity(Entity entity){
        if(entity != null){
            bukkitEntity = entity;
            lastMove = getLocation();
        }
    }

    public Location getLocation(){
        return getBukkitEntity().getLocation();
    }

    public boolean addPermission(Permission permission){
        if(!hasPermission(permission)){
            permissions.add(permission);
            return true;
        }
        return false;
    }

    public boolean hasPermission(Permission permission){
        for(int i = permissions.size() -1; i >= 0; i--){
            Permission permission1 = permissions.get(i);
            if(permission1.getId() == permission.getId()){
                return true;
            }
        }
        return false;
    }

    public void removePermission(Permission permission) {
        for (int i = permissions.size() - 1; i >= 0; i--) {
            Permission permission1 = permissions.get(i);
            if (permission1.getId() == permission.getId()) {
                permissions.remove(i);
            }
        }
    }

    public void Tick() {
        onTick();

        if (offlineData) {
            return;
        }

        if ( tickCounter % 7 == 0) {
            checkMovement();
        }
        tickCounter+=1;

    }

    /**
     * 動いているか
     * @return
     */
    public void checkMovement() {
        double lastX = getLocation().getX();
        double lastY = getLocation().getY();
        double lastZ = getLocation().getZ();

        isMoving = getLocation().getX() != lastX || getLocation().getZ() != lastZ;

    }




    public void runNoDamageTick(int sec){
        this.setMuteki(true);

        new BukkitRunnable() {

            @Override
            public void run() {
                setMuteki(false);
            }
        }.runTaskLater(SimpleCore.simpleCore, 20 * sec);
    }






}

package net.nekonigiri.api.nekonigiricore.enums;

import org.bukkit.ChatColor;

public enum Permission {

    ADMIN(1,"Admin",5, ChatColor.RED),
    MODERATOR(2,"Moderator",4, ChatColor.AQUA),
    VIP(3,"Vip",2, ChatColor.GOLD),
    LEADERDEVELOPER(10,"Developer",5,ChatColor.GRAY),
    DEVELOPER(4,"Developer",4, ChatColor.GRAY),
    MAPPER(5,"Mapper", 1, ChatColor.LIGHT_PURPLE),
    YOUTUBE(6,"Youtube", 2, ChatColor.DARK_RED),
    BUILDER(7,"Builder",2, ChatColor.GRAY),
    MAPMODERATOR(8,"MapModerator",4, ChatColor.DARK_AQUA),
    JrMODERATOR(9,"JrModerator",3, ChatColor.DARK_AQUA),
    MEMBER(0,"Member",1, ChatColor.YELLOW);

    private int id;
    private String name;
    private int level;//レベル
    private ChatColor permissionColor;

    Permission(int id, String name, int level, ChatColor permColor){
        this.name=name;
        this.id = id;
        this.level = level;
        this.permissionColor=permColor;
    }
    public int getLevel(){
        return level;
    }

    public String getName(){
        return name;
    }

    public int getId() {
        return id;
    }

    public String getPrefix(){
        return ""+permissionColor+ChatColor.BOLD+this.getName()+ChatColor.RESET+" ";
    }

    /**
     * Defaultはnormalを返す
     * @param enums
     * @return
     */
    public static Permission getPermission(String enums){
        for(Permission perm:Permission.values()){
            if(perm.name.equalsIgnoreCase(enums)){
                return perm;
            }
        }
        return Permission.MEMBER;
    }

    public static Permission getPermission(int id){
        for(Permission perm:Permission.values()){
            if(perm.getId() == id){
                return perm;
            }
        }
        return Permission.MEMBER;
    }

}

package net.nekonigiri.api.nekonigiricore.enums;

public enum  Language {
    Japanese("ja_JP"),
    English("en_US"),
    Chinese("zh_CH");

    private String name;

    //コンストラクタ
    Language(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
    public static Language getLanguage(String name){
        String search = name.toLowerCase();
        Language[] languages = Language.values();
        for(Language lan:languages){
            if(lan.getName().equalsIgnoreCase(search)){
                return lan;
            }
        }
        return Language.English;
    }
    public String toString(){
        return name();
    }

}
